package com.ss.evaluacion;



import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    private TextView tvBusqueda;
    private TextView tvIngredients;
    private Button btSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tvBusqueda = (TextView) findViewById(R.id.TextB);
        this.btSearch = (Button) findViewById(R.id.BtSearch);
        this.tvIngredients = (TextView) findViewById(R.id.tvIngredients);

        this.btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b = tvBusqueda.getText().toString().trim();
                String url = "https://api.edamam.com/search?q="+b+"&app_id=a0ab577b&app_key=a12f235aa736110dc99557f24cb320e1";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            public void onResponse(String response) {
                                //respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);


                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                    String ingre = "" ;

                                    for(int i = 0; i<in.length();i++){

                                        ingre = ingre + in.getString(i)+ "\n" ;


                                    }
                                    tvIngredients.setText(ingre);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Error
                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);
            }
        });
    }
}
